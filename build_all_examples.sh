#!/bin/bash

set -e

TARGET="--target=wasm32-unknown-unknown"

echo "Build all ./examples"
cargo +nightly build $TARGET --manifest-path=./examples/counter/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/counter-component/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/counter-nested-component/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/sub-app/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/fetch-json/Cargo.toml
cargo +nightly build $TARGET --manifest-path=./examples/todo/Cargo.toml
