#![feature(proc_macro_hygiene)]
use simi::prelude::*;
use wasm_bindgen::prelude::*;

struct ParentComponent<'a, A: Application> {
    message: &'static str,
    child1: Box<Component<A> + 'a>,
    child2: Box<Component<A> + 'a>,
}

impl<A: Application> Component<A> for ParentComponent<'_, A> {
    fn render(&self, context: RenderContext<A>) {
        component! {
            //@debug
            fieldset {
                legend { "This is a `ParentComponent`" }
                p {
                    // Without `$` prefix, this will be process as a Text node
                    self.message
                }
                // You must prefix a child component with `$`
                $ self.child1
                $ self.child2
            }
        }
    }
}

struct Counter<'a, A: Application> {
    title: &'static str,
    value: &'a i32,
    up: ElementEventHandler<A>,
    down: ElementEventHandler<A>,
}

impl<A: Application> Component<A> for Counter<'_, A> {
    fn render(&self, context: RenderContext<A>) {
        component! {
            fieldset {
                legend { "This is a `Counter` component" }
                p { #self.title self.value }
                button (onclick=#self.down) { "Down" }
                button (onclick=#self.up) { "Up" }
            }
        }
    }
}

#[simi_app]
struct Counters {
    month: i32,
    year: i32,
}

enum Msg {
    MonthUp,
    MonthDown,
    YearUp,
    YearDown,
}

impl Application for Counters {
    type Message = Msg;
    type Context = ();
    type Parent = ();
    fn init() -> Self {
        Self {
            month: 12,
            year: 2018,
        }
    }
    fn update(&mut self, m: Self::Message, _: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::MonthUp => {
                self.month += 1;
                if self.month > 12 {
                    self.year += 1;
                    self.month = 1;
                }
            }
            Msg::MonthDown => {
                self.month -= 1;
                if self.month < 1 {
                    self.year -= 1;
                    self.month = 12;
                }
            }
            Msg::YearUp => self.year += 1,
            Msg::YearDown => self.year -= 1,
        }
        true
    }
    fn render(&self, context: RenderContext<Self>) {
        application! {
            //@debug
            h1 { "Counters" }
            p { "using " b {"nested components"} }
            hr
            ParentComponent {
                message: "This component renders two child-components:",
                child1: Counter {
                    title: "Month: ",
                    value: &self.month,
                    up: onclick=#Msg::MonthUp,
                    down: onclick=#Msg::MonthDown,
                }
                child2: Counter {
                    title: "Year: ",
                    value: &self.year,
                    up: onclick=#Msg::YearUp,
                    down: onclick=#Msg::YearDown,
                }
            }
        }
    }
}
