#![feature(proc_macro_hygiene)]
use serde_derive::Deserialize;
use simi::fetch::*;
use simi::prelude::*;
use wasm_bindgen::prelude::*;

#[derive(Deserialize)]
struct Commit {
    id: String,
    short_id: String,
    title: String,
    created_at: String,
    message: String,
    author_name: String,
    author_email: String,
    authored_date: String,
}

#[derive(Deserialize)]
struct Branch {
    name: String,
    commit: Commit,
}

enum Msg {
    ButtonClick,
    BranchInfoLoaded(Result<Branch, simi::error::ErrorString>),
}

pub struct Context {
    main: WeakMain<AppState>,
}

impl AppContext<AppState> for Context {
    fn init(main: WeakMain<AppState>, _parent: ()) -> Self {
        Self { main }
    }
}

impl Context {
    fn get_simi_master_branch_info(&mut self) -> String {
        let url = "https://gitlab.com/api/v4/projects/9547444/repository/branches/master";
        let cb = simi::callback::with_arg(self.main.clone(), Msg::BranchInfoLoaded);
        let f = FetchBuilder::new("GET", url);
        match f.fetch_json(cb) {
            Ok(_) => "Fetch request sent for simi's master branch info".to_string(),
            Err(e) => format!("Error on start fetching {}: {}", url, e),
        }
    }
}

#[simi_app]
struct AppState {
    message: String,
    master_branch_info: Option<Branch>,
}

impl Application for AppState {
    type Message = Msg;
    type Context = Context;
    type Parent = ();

    fn init() -> Self {
        Self {
            message: "Click the button, and wait for a moment".to_string(),
            master_branch_info: None,
        }
    }

    fn update(&mut self, m: Self::Message, cp: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::ButtonClick => {
                self.message = cp.context.get_simi_master_branch_info();
            }
            Msg::BranchInfoLoaded(rs) => match rs {
                Ok(rs) => {
                    self.master_branch_info = Some(rs);
                    self.message = "simi's master branch info is now available".to_string();
                }
                Err(e) => self.message = e.into(),
            },
        }
        true
    }

    fn render(&self, context: RenderContext<Self>) {
        application! {
            //@debug
            h1 { "Simi latest commit info" }
            if let Some(ref bi) = self.master_branch_info {
                table {
                    tr {
                        th { "Branch" }
                        td { bi.name }
                    }
                    tr {
                        th { "Commit id" }
                        td { bi.commit.id }
                    }
                    tr {
                        th { "Commit short id" }
                        td { bi.commit.short_id }
                    }
                    tr {
                        th { "Title" }
                        td { bi.commit.title }
                    }
                    tr {
                        th { "Created at" }
                        td { bi.commit.created_at }
                    }
                    tr {
                        th { "Message" }
                        td { bi.commit.message }
                    }
                    tr {
                        th { "Author name" }
                        td { bi.commit.author_name }
                    }
                    tr {
                        th { "Author email" }
                        td { bi.commit.author_email }
                    }
                    tr {
                        th { "Authored data" }
                        td { bi.commit.authored_date }
                    }
                }
            } else {
                button (onclick=Msg::ButtonClick) { "Fetch" }
                p { self.message }
            }
        }
    }
}
