pub struct TodoItem {
    pub title: String,
    pub completed: bool,
}

pub struct TodoList(Vec<TodoItem>);

impl TodoList {
    pub fn new() -> Self {
        TodoList(Vec::new())
    }

    pub fn completed_count(&self) -> usize {
        self.0.iter().fold(
            0,
            |count, item| if item.completed { count + 1 } else { count },
        )
    }

    pub fn add(&mut self, title: &str) {
        self.0.push(TodoItem {
            title: title.to_string(),
            completed: false,
        });
    }

    pub fn remove(&mut self, index: usize) {
        self.0.remove(index);
    }

    pub fn toggle(&mut self, index: usize) {
        let task = &mut self.0[index];
        task.completed = !task.completed;
    }

    pub fn toggle_all(&mut self, checked: bool) -> bool {
        if self.0.is_empty() {
            return false;
        }
        self.0.iter_mut().for_each(|item| item.completed = checked);
        true
    }

    pub fn clear_completed(&mut self) -> bool {
        if self.completed_count() == 0 {
            false
        } else {
            let left = self.0.drain(..).filter(|i| !i.completed).collect();
            self.0 = left;
            true
        }
    }

    pub fn list(&self) -> &Vec<TodoItem> {
        &self.0
    }

    pub fn update_title(&mut self, index: usize, title: String) {
        self.0[index].title = title;
    }
}
