# Special attributes
There are some attributes that are treated specially in Simi. One of the special attribute, as you may know now, is `conditional class` (see: [macros](macros.md)).

## `<select>`'s value

You can provide normal value to the select's value, and it must have `impl ToString`:
```rust
application! {
    select (value=self.normal_value) {
        // <option>s here
    }
}
```

You can also provide an `Option<String>` to the select's value, but the expression is required to be prefixed by a **question mark** `?`.
```rust
application! {
    select (value=?self.optional_value) {
        //        ^ it requires a question mark

        // <option>s here
    }
}
```

## `<select>`'s index

Similarly, Simi offer the `index` attribute with the required type is: `usize` or `Option<usize>`.
```rust
application! {
    select (index=self.usize_index) {
        // <option>s here
    }
}
```

Or
```rust
application! {
    select (index=?self.optional_usize_index) {
        //        ^ it requires a question mark

        // <option>s here
    }
}
```

## `<textarea>`'s value

In Simi, you must provide the content for a `textarea` via an attribute:
```rust
application! {
    textarea (value=self.some_value)
}
```