pub fn is_html_element_tag(name: &str) -> bool {
    match name {
        "address" | "article" | "aside" | "footer" | "header" | "h1" | "h2" | "h3" | "h4"
        | "h5" | "h6" | "hgroup" | "nav" | "section" | "blockquote" | "dd" | "div" | "dl"
        | "dt" | "figcaption" | "figure" | "hr" | "li" | "main" | "ol" | "p" | "pre" | "ul"
        | "a" | "abbr" | "b" | "bdi" | "bdo" | "br" | "cite" | "code" | "data" | "dfn" | "em"
        | "i" | "kbd" | "mark" | "q" | "rp" | "rt" | "rtc" | "ruby" | "s" | "samp" | "small"
        | "span" | "strong" | "sub" | "sup" | "time" | "u" | "var" | "wbr" | "area" | "audio"
        | "img" | "map" | "track" | "video" | "embed" | "iframe" | "object" | "param"
        | "picture" | "source" | "canvas" | "noscript" | "script" | "del" | "ins" | "caption"
        | "col" | "colgroup" | "table" | "tbody" | "td" | "tfoot" | "th" | "thead" | "tr"
        | "button" | "datalist" | "fieldset" | "form" | "input" | "label" | "legend" | "meter"
        | "optgroup" | "option" | "output" | "progress" | "select" | "textarea" | "details"
        | "dialog" | "menu" | "menuitem" | "summary" | "slot" | "template" => true,
        _ => false,
    }
}

pub fn has_body_content(name: &str) -> bool {
    match name {
        "input" | "textarea" | "img" => false,
        _ => true,
    }
}

pub fn is_html_element_event(name: &str) -> bool {
    match name {
        "onfocus"
        | "onblur"
        | "onreset"
        | "onsubmit"
        | "onkeydown"
        | "onkeypress"
        | "onkeyup"
        | "onmouseenter"
        | "onmouseover"
        | "onmousemove"
        | "onmousedown"
        | "onmouseup"
        | "onauxclick"
        | "onclick"
        | "ondblclick"
        // Alternative name for "ondblclick"
        | "ondoubleclick"
        | "oncontextmenu"
        | "onwheel"
        | "onmouseleave"
        | "onmouseout"
        | "onselect"
        | "onpointerlockchange"
        | "onpointerlockerror"
        | "oninput"
        | "onchange" => true,
        _ => false,
    }
}

pub fn get_official_html_element_event_name(name: String) -> String {
    match name.as_ref() {
        "ondoubleclick" => "ondblclick".to_string(),
        _ => name,
    }
}

pub fn get_js_event_argument_type(event_name: &str) -> &str {
    match event_name {
        "onblur" | "onfocus" => "FocusEvent",
        "onchange" | "onreset" | "onsubmit" | "onpointerlockchange" | "onpointerlockerror" => {
            "Event"
        }
        "onkeydown" | "onkeypress" | "onkeyup" => "KeyboardEvent",
        "onauxclick" | "onclick" | "ondblclick" | "onmouseenter" | "onmouseover"
        | "onmousemove" | "onmousedown" | "onmouseup" | "onmouseleave" | "onmouseout"
        | "oncontextmenu" => "MouseEvent",
        "onwheel" => "WheelEvent",
        "onselect" => "UiEvent",
        "oninput" => "InputEvent",

        _ => panic!(format!(
            "JS argument name for {} is not supported yet. Please file a bug!",
            event_name
        )),
    }
}

pub fn is_allow_optional(tag_name: &str, attribute_name: &str) -> bool {
    match (tag_name, attribute_name) {
        ("select", "value") => true,
        ("select", "index") => true,
        _ => false,
    }
}

use super::ElementAttributeType;
pub fn get_attribute_type(tag_name: &str, attribute_name: &str) -> Option<ElementAttributeType> {
    match attribute_name {
        "autocapitalize" | "autofocus" | "contenteditable" | "disabled" | "draggable"
        | "hidden" => return Some(ElementAttributeType::Bool),

        "accesskey" | "class" | "dir" | "dropzone" | "id" | "inputmode" | "is" | "itemid"
        | "itemprop" | "itemref" | "itemscope" | "itemtype" | "lang" | "slot" | "spellcheck"
        | "style" | "tabindex" | "title" | "translate" => return Some(ElementAttributeType::String),

        _ => {}
    }
    match tag_name {
        "a" => match attribute_name {
            "download" | "href" | "hreflang" | "ping" | "referrerpolicy" | "rel" | "target"
            | "type" => Some(ElementAttributeType::String),

            _ => None,
        },
        "button" => match attribute_name {
            "contenteditable" | "disabled" | "draggable" | "hidden" => {
                Some(ElementAttributeType::Bool)
            }
            "form" | "formaction" | "formenctype" | "formmethod" | "formnovalidate"
            | "formtarget" | "name" | "type" | "value" => Some(ElementAttributeType::String),
            _ => None,
        },
        "input" => match attribute_name {
            "value" | "form" | "list" | "max" | "min" | "maxlength" | "minlength" | "name"
            | "pattern" | "placeholder" | "size" | "step" | "tabindex" | "type" => {
                Some(ElementAttributeType::String)
            }
            "autocomplete" | "checked" | "readonly" | "required" => {
                Some(ElementAttributeType::Bool)
            }
            _ => None,
        },
        "label" => match attribute_name {
            "for" | "form" => Some(ElementAttributeType::String),
            _ => None,
        },
        "option" => match attribute_name {
            "selected" => Some(ElementAttributeType::Bool),
            "label" | "value" => Some(ElementAttributeType::String),
            _ => None,
        },
        "select" => match attribute_name {
            "index" => Some(ElementAttributeType::Index),
            "value" => Some(ElementAttributeType::String),
            "autocomplete" | "form" | "name" | "size" => Some(ElementAttributeType::String),
            "multiple" | "required" => Some(ElementAttributeType::Bool),
            _ => None,
        },
        "textarea" => match attribute_name {
            "value" => Some(ElementAttributeType::String),
            _ => None,
        },

        _ => None,
    }
}
