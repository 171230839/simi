//! Help your Rust code to send messages back to your simi app
use std::rc::Rc;

use crate::app::{Application, WeakMain};

/// A callback that send a message without data
pub type Callback = Rc<Fn()>;
/// A callback that send a message with data
pub type CallbackArg<T> = Rc<Fn(T)>;

/// Create callback for a handler that requires no argument
pub fn no_arg<A, H>(main: WeakMain<A>, handler: H) -> Callback
where
    A: Application + 'static,
    H: Fn() -> A::Message + 'static,
{
    Rc::new(move || main.send_message(handler()))
}

/// Create callback for a handler that requires an argument
pub fn with_arg<A, T, H>(main: WeakMain<A>, handler: H) -> CallbackArg<T>
where
    A: Application + 'static,
    H: Fn(T) -> A::Message + 'static,
{
    Rc::new(move |value: T| main.send_message(handler(value)))
}
