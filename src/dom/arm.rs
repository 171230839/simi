//! Manages `if`/`match` clause
use super::NodeList;
/// An active arm of `if` or `match`
pub struct Arm {
    // None if self instance is newly created, no arm rendered yet.
    // Some(value): the arm has index=value is currently rendered.
    index: Option<usize>,
    // Nodes of the active arm
    node_list: NodeList,
}

#[allow(clippy::new_without_default)]
impl Arm {
    /// New empty arm
    pub fn new() -> Self {
        Self {
            index: None,
            node_list: NodeList::new(),
        }
    }

    /// Check if the current value of index is the same as expected_index or not
    pub fn is_same_index(&self, expected_index: Option<usize>) -> bool {
        self.index == expected_index
    }

    /// Update the index
    pub fn set_index(&mut self, index: usize) {
        self.index = Some(index);
    }

    /// A mutable reference to the node list
    pub fn node_list_mut(&mut self) -> &mut NodeList {
        &mut self.node_list
    }

    /// Remove all simi nodes, real nodes and return the real node after the last real node of this arm
    pub fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &web_sys::Node,
    ) -> Option<web_sys::Node> {
        self.node_list.remove_and_get_next_sibling(real_parent)
    }

    /// Remove real nodes from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &web_sys::Node) {
        self.node_list.remove_real_node(real_parent)
    }

    /// Get next sibling of the last node
    pub fn get_next_sibling(&self) -> Option<web_sys::Node> {
        self.node_list.get_next_sibling()
    }

    /// Get first real node
    pub fn get_first_real_node(&self) -> Option<&web_sys::Node> {
        self.node_list.get_first_real_node()
    }

    /// Clone everything except for tracked attributes, and for-loop content
    pub(super) fn clone(&self, real_parent: &web_sys::Node) -> Self {
        // Clone the current arm or just create an empty node?
        Self {
            index: self.index,
            node_list: self.node_list.clone(real_parent),
        }
    }

    /// Start cloning self and all childs
    pub(super) fn start_clone(&self) -> Self {
        // Clone the current arm or just create an empty node?
        Self {
            index: self.index,
            node_list: self.node_list.start_clone(),
        }
    }
}
