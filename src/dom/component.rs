//! Component
use super::NodeList;

// FIXME:
// Implement `Component` as a separate struct because the plan is to add
// more featues to `Component` such as `child component`.
// But most methods are the same as `NonKeyedFor` or `NodeList` itself?
// How to avoid such code duplication?

/// Component
pub struct Component {
    node_list: NodeList,
}

#[allow(clippy::new_without_default)]
impl Component {
    /// New Component node
    pub fn new() -> Self {
        Self {
            node_list: NodeList::new(),
        }
    }

    /// Reserve memory with given capacities.
    pub fn reserve(&mut self, nodes_capacity: usize) {
        if nodes_capacity > self.node_list.len() {
            let more_count = nodes_capacity - self.node_list.len();
            self.node_list.nodes_mut().reserve(more_count);
        }
    }

    /// Mutable reference to self.node_list
    pub fn node_list_mut(&mut self) -> &mut NodeList {
        &mut self.node_list
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &web_sys::Node,
    ) -> Option<web_sys::Node> {
        self.node_list.remove_and_get_next_sibling(real_parent)
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &web_sys::Node) {
        self.node_list.remove_real_node(real_parent)
    }

    /// Get next sibling of the last node
    pub fn get_next_sibling(&self) -> Option<web_sys::Node> {
        self.node_list.get_next_sibling()
    }

    /// Get first real node
    pub fn get_first_real_node(&self) -> Option<&web_sys::Node> {
        self.node_list.get_first_real_node()
    }

    /// Clone everything except for tracked attributes, and for-loop content
    pub(super) fn clone(&self, real_parent: &web_sys::Node) -> Self {
        Self {
            node_list: self.node_list.clone(real_parent),
        }
    }

    /// Start cloning self and all childs
    pub(super) fn start_clone(&self) -> Self {
        Self {
            node_list: self.node_list.start_clone(),
        }
    }
}
