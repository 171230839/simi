//! Implement a virtual html element
use super::NodeList;
use crate::app::{Application, WeakMain};
use crate::element_events::{EventHandler, Listener};
use wasm_bindgen::JsCast;

enum AttributeValue {
    String(String),
    //    Usize(usize),
    Bool(bool),
    EventListener(Box<Listener>),
    None,
}

/// A node that is rendered as any HTML Element such as `div`, `span`...
pub struct Element {
    real_element: web_sys::Element,
    attributes: Vec<AttributeValue>,
    node_list: NodeList,
}

impl Element {
    /// Create new virtual element with given tag.
    /// The equivalent real element is also created, but not inserted in real DOM yet.
    pub fn new(tag: &str) -> Self {
        Self {
            real_element: crate::interop::document()
                .create_element(tag)
                .expect("Create new real element"),
            attributes: Vec::new(),
            node_list: NodeList::new(),
        }
    }

    /// Reserve memory with given capacities.
    pub fn reserve(&mut self, attributes_capacity: usize, nodes_capacity: usize) {
        if attributes_capacity > self.attributes.len() {
            let more_count = attributes_capacity - self.attributes.len();
            self.attributes.reserve(more_count);
        }

        if nodes_capacity > self.node_list.len() {
            let more_count = nodes_capacity - self.node_list.len();
            self.node_list.nodes_mut().reserve(more_count);
        }
    }

    /// Borrow mutable self.node_list, and immutable self.real_element
    pub fn node_list_mut_and_real_node(&mut self) -> (&mut NodeList, &web_sys::Node) {
        (&mut self.node_list, self.real_element.as_ref())
    }

    /// Insert real node to correct place in its parent' node list
    pub fn insert_to(&self, real_parent: &web_sys::Node, next_sibling: Option<&web_sys::Node>) {
        crate::error::log_result_error(
            real_parent.insert_before(&self.real_element.as_ref(), next_sibling),
        );
    }

    /// Number of attributes
    pub fn attribute_count(&self) -> usize {
        self.attributes.len()
    }

    /// Number of child nodes
    pub fn node_count(&self) -> usize {
        self.node_list.len()
    }

    /// Set a bool attribute
    pub fn no_update_bool_attribute(&mut self, name: &str, value: bool) {
        if value {
            crate::error::log_result_error(self.real_element.set_attribute(name, ""));
        }
    }

    /// Set/update a bool attribute
    pub fn bool_attribute(&mut self, index: usize, name: &str, value: bool) {
        if index >= self.attributes.len() {
            self.attributes.push(AttributeValue::Bool(value));
            if value {
                crate::error::log_result_error(self.real_element.set_attribute(name, ""));
            }
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::Bool(ref mut current_value) = a {
                if *current_value != value {
                    *current_value = value;
                    if value {
                        crate::error::log_result_error(self.real_element.set_attribute(name, ""));
                    } else {
                        crate::error::log_result_error(self.real_element.remove_attribute(name));
                    }
                }
            } else {
                log::error!("Attribute at given index is not a bool")
            }
        }
    }

    /// Set a string attribute
    pub fn no_update_literal_string_attribute(&mut self, name: &str, value: &str) {
        crate::error::log_result_error(self.real_element.set_attribute(name, value));
    }

    /// Set a string attribute
    pub fn no_update_string_attribute(&mut self, name: &str, value: &impl ToString) {
        crate::error::log_result_error(self.real_element.set_attribute(name, &value.to_string()));
    }

    /// Set/update a string attribute
    pub fn string_attribute(&mut self, index: usize, name: &str, value: &impl ToString) {
        let value = value.to_string();
        if index >= self.attributes.len() {
            crate::error::log_result_error(self.real_element.set_attribute(name, &value));
            self.attributes.push(AttributeValue::String(value));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::String(ref mut current_value) = a {
                if *current_value != value {
                    crate::error::log_result_error(self.real_element.set_attribute(name, &value));
                    *current_value = value;
                }
            }
        }
    }

    // Do not have `no_update_input_checked` here,
    // use `no_update_bool_attribute` instead

    /// Set/update checkbox's checked
    pub fn input_checked(&mut self, index: usize, value: bool) {
        if index >= self.attributes.len() {
            if value {
                crate::error::log_result_error(self.real_element.set_attribute("checked", ""));
            }
            self.attributes.push(AttributeValue::Bool(value));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::Bool(ref mut current_value) = a {
                if *current_value != value {
                    *current_value = value;
                    let input: &web_sys::HtmlInputElement = self.real_element.unchecked_ref();
                    input.set_checked(value);
                }
            } else {
                log::error!("Attribute at given index is not a bool")
            }
        }
    }

    /// Set/update input's value
    pub fn input_value(&mut self, index: usize, value: &impl ToString) {
        let value = value.to_string();
        if index >= self.attributes.len() {
            crate::error::log_result_error(self.real_element.set_attribute("value", &value));
            self.attributes.push(AttributeValue::String(value));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::String(ref mut current_value) = a {
                if *current_value != value {
                    let input: &web_sys::HtmlInputElement = self.real_element.unchecked_ref();
                    input.set_value(&value);
                    *current_value = value;
                }
            } else {
                log::error!("Attribute at given index is not a string")
            }
        }
    }

    /// Set/update select's value
    pub fn select_value(&mut self, index: usize, value: &impl ToString) {
        if index >= self.attributes.len() {
            // For now, just store AttributeValue::None as a placeholder
            self.attributes.push(AttributeValue::None);
        }
        let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
        select.set_value(&value.to_string());
        // For now, value for <select> will always be setted.
        // If the <select> is created with an empty list of <option>, but with some `value`, then filled, later, with
        // a list of <option>s loaded from network (but the `value` is not changed) then the commented code below may
        // not work.
        /*
        let value = value.to_string();
        if index >= self.attributes.len() {
            crate::error::log_result_error(self.real_element.set_attribute("value", &value));
            self.attributes.push(AttributeValue::String(value));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::String(ref mut current_value) = a {
                if *current_value != value {
                    let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
                    select.set_value(&value);
                    *current_value = value;
                }
            } else {
                log::error!("Attribute at given index is not a string")
            }
        }*/
    }

    /// Set/update select's value
    pub fn select_optional_value(&mut self, index: usize, value: &Option<String>) {
        if index >= self.attributes.len() {
            // For now, just store AttributeValue::None as a placeholder
            self.attributes.push(AttributeValue::None);
        }
        let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
        match value {
            None => {
                select.set_selected_index(-1);
            }
            Some(value) => {
                select.set_value(value);
            }
        }
        // For now, value for <select> will always be setted.
        // If the <select> is created with an empty list of <option>, but with some `value`, then filled, later, with
        // a list of <option>s loaded from network (but the `value` is not changed) then the commented code below may
        // not work.
        /*
        if index >= self.attributes.len() {
            let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
            log::info!("select.options = {}", select.options().length());
            match value {
                None => {
                    select.set_selected_index(-1);
                    self.attributes.push(AttributeValue::None);
                }
                Some(value) => {
                    select.set_value(value);
                    //crate::error::log_result_error(
                    //    self.real_element.set_attribute("value", &value),
                    //);
                    self.attributes
                        .push(AttributeValue::String(value.to_string()));
                }
            }
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            match (value, &a) {
                (None, AttributeValue::String(_)) => {
                    let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
                    select.set_selected_index(-1);
                    *a = AttributeValue::None;
                    //log::info!("select.set_selected_index(-1);");
                }
                (Some(value), AttributeValue::None) => {
                    let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
                    select.set_value(value);
                    *a = AttributeValue::String(value.to_string());
                    //log::info!("select.set_value(value);");
                }
                (Some(value), AttributeValue::String(old_value)) => {
                    if value != old_value {
                        let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
                        select.set_value(value);
                        *a = AttributeValue::String(value.to_string());
                        //log::info!("value != old_value");
                    } else {
                        //log::info!("No change");
                    }
                }
                (None, AttributeValue::None) => {
                    // Nothing to do
                    //log::info!("");
                }
                _ => {
                    log::error!("select_optional_value: Attribute type mismatched");
                }
            }
        }*/
    }

    /// Set/update select's value
    pub fn select_index(&mut self, index: usize, value: usize) {
        if index >= self.attributes.len() {
            // For now, just store AttributeValue::None as a placeholder
            self.attributes.push(AttributeValue::None);
        }
        let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
        select.set_selected_index(value as i32);
    }

    /// Set/update select's value
    pub fn select_optional_index(&mut self, index: usize, value: &Option<usize>) {
        if index >= self.attributes.len() {
            // For now, just store AttributeValue::None as a placeholder
            self.attributes.push(AttributeValue::None);
        }
        let select: &web_sys::HtmlSelectElement = self.real_element.unchecked_ref();
        match value {
            None => {
                select.set_selected_index(-1);
            }
            Some(value) => {
                select.set_selected_index(*value as i32);
            }
        }
    }

    /// Set value for textarea
    pub fn no_update_textarea_value(&mut self, value: &str) {
        let ta: &web_sys::HtmlTextAreaElement = self.real_element.unchecked_ref();
        ta.set_value(value);
    }

    /// Set/update textarea's value
    pub fn textarea_value(&mut self, index: usize, value: &str) {
        // Text inside a textarea tends to be big!
        // Is there a better way to implement this?
        // Maybe: not store it in the self.attributes?
        // But check against the self.real_element.value() directly?

        if index >= self.attributes.len() {
            let ta: &web_sys::HtmlTextAreaElement = self.real_element.unchecked_ref();
            ta.set_value(&value);
            self.attributes
                .push(AttributeValue::String(value.to_string()));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::String(ref mut current_value) = a {
                if *current_value != value {
                    let input: &web_sys::HtmlTextAreaElement = self.real_element.unchecked_ref();
                    input.set_value(value);
                    *current_value = value.to_string();
                }
            } else {
                log::error!("Attribute at given index is not a string")
            }
        }
    }

    /// Set a conditional class
    pub fn no_update_conditional_class(&mut self, class: &str, on: bool) {
        if on {
            crate::error::log_result_error(self.real_element.class_list().add_1(class));
        }
    }

    /// Set/update the conditional class
    pub fn conditional_class(&mut self, index: usize, class: &str, on: bool) {
        if index >= self.attributes.len() {
            if on {
                crate::error::log_result_error(self.real_element.class_list().add_1(class));
            }
            self.attributes.push(AttributeValue::Bool(on));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::Bool(ref mut current_value) = a {
                if *current_value != on {
                    *current_value = on;
                    if on {
                        crate::error::log_result_error(self.real_element.class_list().add_1(class));
                    } else {
                        crate::error::log_result_error(
                            self.real_element.class_list().remove_1(class),
                        );
                    }
                }
            } else {
                log::error!("Attribute (expected a conditional class) at given index is not a bool")
            }
        }
    }

    /// Attach/update event handler
    pub fn event_handler<A: Application>(
        &mut self,
        index: usize,
        main: &WeakMain<A>,
        mut event_handler: Box<EventHandler<A>>,
    ) {
        let et: &web_sys::EventTarget = self.real_element.as_ref();
        if index >= self.attributes.len() {
            let el = event_handler.create_event_listener(main);
            crate::error::log_result_error(
                et.add_event_listener_with_callback(el.event_name(), el.js_function()),
            );
            self.attributes.push(AttributeValue::EventListener(el));
        } else {
            let a = unsafe {
                // It is safe here because
                //      index < self.attributes.len()
                // is true here
                self.attributes.get_unchecked_mut(index)
            };
            if let AttributeValue::EventListener(ref mut current_listener) = a {
                crate::error::log_result_error(et.remove_event_listener_with_callback(
                    current_listener.event_name(),
                    current_listener.js_function(),
                ));
                let new_listener = event_handler.create_event_listener(main);
                crate::error::log_result_error(et.add_event_listener_with_callback(
                    new_listener.event_name(),
                    new_listener.js_function(),
                ));
                *current_listener = new_listener;
            } else {
                log::error!("Not an AttributeValue::EventListener");
            }
        }
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &web_sys::Node,
    ) -> Option<web_sys::Node> {
        let next_sibling: Option<web_sys::Node> = {
            let node: &web_sys::Node = self.real_element.as_ref();
            node.next_sibling()
        };
        crate::error::log_result_error(real_parent.remove_child(self.real_element.as_ref()));
        next_sibling
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &web_sys::Node) {
        crate::error::log_result_error(real_parent.remove_child(&self.real_element.as_ref()));
    }

    /// Get next sibling of the last node
    pub fn get_next_sibling(&self) -> Option<web_sys::Node> {
        let node: &web_sys::Node = self.real_element.as_ref();
        node.next_sibling()
    }

    /// Get first real node
    pub fn get_first_real_node(&self) -> Option<&web_sys::Node> {
        Some(self.real_element.as_ref())
    }

    /// Clone everything except for tracked attributes, and for-loop content
    pub(super) fn clone(&self, real_parent: &web_sys::Node) -> Self {
        let clone = self.start_clone();
        crate::error::log_result_error(real_parent.append_child(clone.real_element.as_ref()));
        clone
    }

    /// Start cloning self and all childs
    pub(super) fn start_clone(&self) -> Self {
        let real_element: web_sys::Element = self
            .real_element
            .clone_node_with_deep(false)
            .expect("clone real_element")
            .unchecked_into();
        let node_list = self.node_list.clone(real_element.as_ref());
        Self {
            real_element,
            attributes: Vec::with_capacity(self.attributes.capacity()),
            node_list,
        }
    }
}
