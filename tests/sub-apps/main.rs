#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

use simi::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

mod sub_app;

#[simi_app]
pub struct MainApp {
    value: i32,
}

pub enum MainMsg {
    Delta(i32),
}

impl Application for MainApp {
    type Message = MainMsg;
    type Context = ();
    type Parent = ();

    fn init() -> Self {
        Self { value: 2018 }
    }

    fn update(&mut self, m: Self::Message, _cp: ContextPlus<Self>) -> UpdateView {
        match m {
            MainMsg::Delta(value) => self.value += value,
        }
        true
    }

    fn mounted(&self, cp: ContextPlus<Self>) {
        cp.sub_apps
            .get::<crate::sub_app::SubApp>(2)
            .expect("SubApp 1")
            .send_message(crate::sub_app::Msg::SetDelta(10))
    }

    fn render(&self, context: RenderContext<Self>) {
        application! {
            //@debug
            div (id="value") { "Value: " self.value }
            div(id="app1") {
                @sub_app(1, crate::sub_app::SubApp)
            }
            div(id="app2") {
                @sub_app(2, crate::sub_app::SubApp)
            }
        }
    }
}

#[wasm_bindgen_test]
fn sub_app() {
    let _main: RcMain<MainApp> = simi_test::start_app();
    let value = simi_test::Element::id("value");
    let button1 = simi_test::Element::selector("#app1 button");
    let button2 = simi_test::Element::selector("#app2 button");

    assert_eq!(
        "Value: 2018",
        value.as_node().text_content().expect("Text content")
    );

    button1.click();
    assert_eq!(
        "Value: 2019",
        value.as_node().text_content().expect("Text content")
    );

    button2.click();
    assert_eq!(
        "Value: 2029",
        value.as_node().text_content().expect("Text content")
    );
}
