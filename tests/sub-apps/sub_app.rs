use crate::{MainApp, MainMsg};
use simi::prelude::*;

pub struct Context {
    parent: WeakMain<MainApp>,
}

impl AppContext<SubApp> for Context {
    fn init(_main: WeakMain<SubApp>, parent: WeakMain<MainApp>) -> Self {
        Self { parent }
    }
}

pub enum Msg {
    SetDelta(i32),
    ButtonClick,
}

pub struct SubApp {
    delta: i32,
}

impl Application for SubApp {
    type Message = Msg;
    type Context = Context;
    type Parent = WeakMain<MainApp>;

    fn init() -> Self {
        SubApp { delta: 1 }
    }

    fn update(&mut self, m: Self::Message, cp: ContextPlus<Self>) -> UpdateView {
        match m {
            Msg::SetDelta(value) => self.delta = value,
            Msg::ButtonClick => cp.context.parent.send_message(MainMsg::Delta(self.delta)),
        }
        false
    }

    fn render(&self, context: RenderContext<Self>) {
        application! {
            button (onclick=Msg::ButtonClick) { "Click me" }
        }
    }
}
